#pragma once
#include <netinet/in.h>
#include "IpSocket.h"

namespace network {

struct FromTcpAddres
{
	sockaddr addr{0};
	socklen_t len{sizeof(addr)};
};

class TcpSocket : public IpSocket
{
public:
	TcpSocket(Info const &si)
	    : IpSocket(si)
	    , peer(){};

	virtual SocketPtr accept() override;
	virtual sockaddr const *fromAddr(socklen_t *addr_len) const override;

private:
	TcpSocket(TcpSocket const *const server);

	virtual int onOpen(ListIpAddress const &list_ip) override;

	void bindToAddress(int fd, ListIpAddress const &list_ip);
	void connectToAddress(int fd, ListIpAddress const &list_ip);

	std::unique_ptr<FromTcpAddres> peer;  ///< Used for server if TcpSocket created on accept
};

}  // namespace network
