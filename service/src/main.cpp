#include <iostream>
#include <thread>
#include "models/SimpleData.h"
#include "models/StringData.h"
#include "config.h"

void sendThread();
void listenThread();

void onObjectReceived(models::SimpleData &&data)
{
	std::cout << data;
}

void onObjectReceived(models::StringData &&data)
{
	std::cout << data;
}

int main(int argc, char *argv[])
{
	auto &config = Config::instance(argc, argv);

	std::thread recv;
	if(!config.local_address.empty())
		recv = std::thread(&listenThread);

	std::thread send;
	if(!config.remote_address.empty())
		send = std::thread(&sendThread);

	if(send.joinable())
		send.join();
	if(recv.joinable())
		recv.join();

	return 0;
}
