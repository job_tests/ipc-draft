#pragma once
#include <string_view>

namespace tag {

uint16_t constexpr DataVersion = 1;

std::string_view constexpr Version = "version";
std::string_view constexpr ClientName = "client_name";
std::string_view constexpr ClientId = "client_id";
std::string_view constexpr Exit = "exit";

}  // namespace tag
