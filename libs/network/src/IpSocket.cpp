#include <cassert>
#include <netdb.h>
#include <arpa/inet.h>
#include "network/IpSocket.h"

namespace network {

namespace {

int extractPort(std::string *address)
{
	assert(address && "Pointer to address string required");
	int port = -1;
	auto const port_splitter = address->find_last_of(':');
	if(port_splitter == std::string::npos)
		return port;
	std::string str_port(address->substr(port_splitter + 1));
	try {
		port = std::stoi(str_port);
	} catch(...) {
		throw SocketException({"Wrong formated TCP address:port='%'", *address});
	}

	address->resize(port_splitter);
	return port;
}

ListIpAddress resolveAddress(std::string const &address)
{
	in_addr ip = {INADDR_NONE};
	if(inet_aton(address.c_str(), &ip))
		return {ip.s_addr};
	hostent *he = gethostbyname(address.c_str());
	if(!he)
		throw SocketException({"Can't resolve IP address from '%'", address});

	in_addr **addr_list = reinterpret_cast<in_addr **>(he->h_addr_list);
	ListIpAddress list_ip;
	list_ip.reserve(he->h_length / (sizeof(in_addr_t) - 1));
	while(*addr_list) {
		list_ip.emplace_back((*addr_list)->s_addr);
		++addr_list;
	}

	if(list_ip.size() == 0)
		throw SocketException({"No resolved IP address from '%'", address});
	return list_ip;
}

}  // namespace

IpSocket::IpSocket(Info const &si)
    : Socket(si)
    , addr({AF_INET})
    , address(si.address)
    , server_backlog(si.server_backlog)
{
	if(!address.empty()) {
		int port = si.port > 0 ? si.port : extractPort(&address);
		if(port <= 0)
			throw SocketException("Not specified IP port");
		addr.sin_port = htons(port);

	} else
		addr.sin_port = si.port;
}

/*virtual*/ void IpSocket::open() /*override*/
{
	ListIpAddress list_ip = resolveAddress(address);
	int fd = onOpen(list_ip);
	Socket::open(fd);
}

};  // namespace network
