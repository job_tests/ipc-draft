#include "channel/BinaryEncoder.h"

namespace channel::binary {

Encoder::Encoder(SenderPtr sender, int expected_size /*=0*/)
    : BaseEncoder(sender)
    , buffer()
{
	int const MaxMsgSize = sender->maxMessageSize();
	buffer.reserve(expected_size < MaxMsgSize ? expected_size : MaxMsgSize);
}

/*virtual*/ Encoder::~Encoder()
{
	checkSend();
}

void Encoder::send()
{
	sendBatch();
}

/*virtual*/ void Encoder::add(void const *data, size_t len)
{
	auto ptr = static_cast<char const *>(data);
	buffer.insert(buffer.end(), ptr, ptr + len);
}

/*virtual*/ void Encoder::sendBatch()
{
	checkSend();
}

void Encoder::checkSend() noexcept
{
	if(!buffer.empty()) {
		if(sender)
			sender->send(buffer.data(), buffer.size());
		buffer.clear();
	}
}

/*virtual*/ void Encoder::append(void const *data, size_t len, std::string_view key) /*override*/
{
	// FIXME(gertsog@arrival.com): output << reserve(size_of_entry) << val1 << val2 << val3 << end;
	// TODO(gertsog@arrival.com): нужна защита от отправки не законченного элемента:
	// * Если при обработке reserve() size_of_entry превышает допустимый размер - отправляем то что есть
	// * Если делается append с превышением reserved размера И оно не помещается в буфер:
	// *   делаем roolback на сохраненную позицию в момент вызова reserve()
	// *   с копированием всех новых данных во временный буфер и после sendBatch() накатываем скопированные данные
	// * buffer.size() + len + virtual int suffixSize() -> чтобы учитывать место для завершения пакета в sendBatch()
	// * output << end - выключает режим "защиты". Может отсутствовать:
	// *   в этом случае последующий вызов reserve() или деструктор работает аналогично end
	// * reserve() && end не требуются если append() делается "атомарно" одним буфером для всего элемента
	int const DataSize = buffer.size() + len + (key.empty() ? 0 : key.size() + 1);
	if(sender && DataSize >= sender->maxMessageSize())
		sendBatch();

	if(!key.empty()) {
		add(key.data(), key.size());
		add("\0", 1);
	}

	if(len)
		add(data, len);
}

}  // namespace channel::binary
