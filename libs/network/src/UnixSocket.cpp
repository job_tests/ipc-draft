#include <errno.h>
#include <string.h>
#include <cassert>
#include <sys/socket.h>
#include <sys/stat.h>

#include "network/UnixSocket.h"

namespace network {

UnixSocket::UnixSocket(Info const &si)
    : Socket(si)
    , addr({AF_UNIX})
{
	strncpy(addr.sun_path, si.address.c_str(), sizeof(addr.sun_path) - 1);
	if(si.address[0] == AbstractUnixSocketSign)
		addr.sun_path[0] = '\0';
}

/*virtual*/ void UnixSocket::open() /*override*/
{
	int fd = makeSocket(AF_UNIX, SOCK_STREAM, 0);
	if(connect(fd, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) != 0) {
		close(fd);
		throw SocketException({"Can't connect socket: ", strerror(errno)});
	}
	Socket::open(fd);
}

/*static*/ bool UnixSocket::is_socket(std::string const &address)
{
	if(address.length()) {
		if(address[0] == AbstractUnixSocketSign)
			return true;
		struct stat file_info;
		if(stat(address.c_str(), &file_info) == 0)
			return true;
	}
	return false;
}

}  // namespace network
