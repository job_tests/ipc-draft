#pragma once
#include <cassert>
#include "Exception.h"
#include "BaseEncoders.h"

namespace channel::binary {

class Decoder final : public BaseDecoder
{
public:
	Decoder(void const *const data, size_t len)
	    : buffer(static_cast<char const *const>(data))
	    , data_size(len)
	    , offset()
	{}

	virtual char const *end() const noexcept override { return &buffer[offset]; }

	virtual int length() const noexcept override { return data_size - offset; }

	virtual void shift(size_t element_size) override
	{
		offset += element_size;
		assert(offset <= data_size && "Offset exceed data size");
		if(offset > data_size)
			throw Exception({"Offset=% exceed data size=%", offset, data_size});
	}

	virtual bool findEntry(std::string_view key) override
	{
		if(!key.empty()) {
			if(length() <= key.size())
				throw Exception(
				    {"Data size=% less then key size=%; Total data size=%", length(), key.size(), data_size});

			if(key != end())
				return false;
			// TODO(gertsog@gmail.com): Need handle case when tag in buffer just starts with key, but longer (!)
			shift(key.size() + 1);
		}
		return true;
	}

private:
	char const *const buffer;
	size_t const data_size;
	size_t offset;
};

}  // namespace channel::binary
