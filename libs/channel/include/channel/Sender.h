#pragma once
#include <cstddef>
#include <memory>
#include <vector>

namespace channel {

class Sender
{
	static int constexpr DefaultMaxMessageSize = 0x10000;

public:
	virtual ~Sender() {}

	virtual void send(void const *data, size_t len) = 0;

	virtual int maxMessageSize() const noexcept { return DefaultMaxMessageSize; }
};

using SenderPtr = std::shared_ptr<Sender>;

}  // namespace channel
