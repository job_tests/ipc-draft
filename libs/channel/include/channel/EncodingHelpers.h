#pragma once
#include <cassert>
#include <string>
#include <type_traits>
#include "is_vector.h"
#include "Exception.h"
#include "BaseEncoders.h"

namespace channel::encoding {

template<typename T>
typename std::enable_if_t<std::is_arithmetic<T>::value, void> encode(BaseEncoder &output, T val, Value<T> v)
{
	output.append(&val, sizeof(val), v.key);
}

template<typename T>
typename std::enable_if_t<std::is_arithmetic<T>::value, T> decode(BaseDecoder &input, Value<T> v)
{
	if(!input.findEntry(v.key))
		throw Exception({"Wrong key: '%' != '%'", v.key, input.end()});

	T val;
	memcpy(&val, input.end(), sizeof(val));
	input.shift(sizeof(val));
	return val;
}

template<typename T>
typename std::enable_if_t<std::is_enum<T>::value, void> encode(BaseEncoder &output, T const val, Value<T> v)
{
	using EnumType = typename std::underlying_type<T>::type;
	output.save(static_cast<EnumType>(val), v.key);
}

template<typename T>
typename std::enable_if_t<std::is_enum<T>::value, T> decode(BaseDecoder &input, Value<T> v)
{
	using EnumType = typename std::underlying_type<T>::type;
	EnumType value = input.load<EnumType>(v.key);
	return static_cast<T>(value);
}

using StringLengthType = uint16_t;
template<typename T>
typename std::enable_if_t<std::is_constructible<std::string_view, T>::value, void> encode(BaseEncoder &output,
                                                                                          T const &data,
                                                                                          Value<T> v)
{
	std::string_view str(data);
	output.save(static_cast<StringLengthType>(str.size()), v.key);
	output.append(str.data(), str.size(), {});
}

std::string decode(BaseDecoder &input, Value<std::string> v);

template<typename T>
typename std::enable_if_t<is_vector_v<T>, T> decode(BaseDecoder &input, Value<T> v)
{
	using ElementType = typename T::value_type;
	T list;
	auto const size = input.load<uint32_t>(v.key);
	list.reserve(size);
	for(size_t i = 0; i < size; ++i)
		list.push_back(std::move(input.load<ElementType>({})));
	return list;
}

template<typename T>
typename std::enable_if_t<is_vector_v<T>, void> encode(BaseEncoder &output, T const &data, Value<T> v)
{
	// TODO(gertsog@gmail.com): Possible would be better add: BaseEncoder::start[Object|Array](key)
	output.save(uint32_t(data.size()), v.key);
	for(auto &val : data)
		output.save(val, {});
}

}  // namespace channel::encoding
