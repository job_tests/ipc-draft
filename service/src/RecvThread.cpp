#include <iostream>
#include <thread>
#include "network/Socket.h"
#include "channel/BinaryDecoder.h"
#include "models/SimpleData.h"
#include "models/StringData.h"
#include "config.h"
#include "logger.h"
#include "prototcol_tags.h"

template<typename T>
void callbackAdapter(T &&value)
{
	void onObjectReceived(T &&);  // Forward declaration extern function with argument type T
	onObjectReceived(std::move(value));
}

namespace {

int constexpr BufferSize = 0x1000;

bool parseBuffer(channel::BaseDecoder &&decoder)
{
	// TODO(gertsog@gmail.com): To make more flexible need use some kind of map<string, typename>
	while(decoder.length() > 0) {
		VERBOSE(2, "Tag='%'", decoder.end());

		if(models::StringData::DefaultName == decoder.end())
			callbackAdapter(decoder.load<models::StringData>(models::StringData::DefaultName));

		else if(models::SimpleData::DefaultName == decoder.end())
			callbackAdapter(decoder.load<models::SimpleData>(models::SimpleData::DefaultName));

		else if(tag::Version == decoder.end())
			std::cout << "Version: " << decoder.load<uint16_t>(tag::Version) << std::endl;

		else if(tag::ClientName == decoder.end())
			std::cout << "ClientName: " << decoder.load<std::string>(tag::ClientName) << std::endl;

		else if(tag::ClientId == decoder.end())
			std::cout << "ClientId: " << decoder.load<pid_t>(tag::ClientId) << std::endl;

		else if(tag::Exit == decoder.end()) {
			std::cout << "Exit ClientName: " << decoder.load<std::string>(tag::Exit) << std::endl;
			return false;
		}

		VERBOSE(2, "Rest of decoder data % bytes.", decoder.length());
	}
	return true;
}

/**
 * TODO(gertsog@gmail.com): need add handling continuous flow of TCP stream:
 * 	  1) need add header in channel::binary::Encoder where would be at least a size field of the entire packet.
 *  Option A - read as much as posible
 * 	  2) on parsing stream data do checks if header is received completely and it's packet body, too.
 * 	  3) if packet was splitted by limits of buffer, then rest part of data (from last header to end of buffer)
 *      should be moved at the beginning and call next recv() with offset and reduced size.
 *
 *  Option B - read by small chunks of packets
 *    2) Read packet's header, extract body size value
 *    3) Read following body with size from p.2 and parse it
 *    4) Repeat p.2-3 until have any data on recv()
 */
void recvThread(network::SocketPtr socket)
{
	char buffer[BufferSize];
	while(true) {
		size_t recv_bytes = socket->recv(buffer, sizeof(buffer));
		VERBOSE(1, "<<< Receive % bytes. >>>", recv_bytes);
		assert(recv_bytes < sizeof(buffer) && "NOTE: here we are expecting all data should fits in one packet");

		if(!recv_bytes || !parseBuffer(channel::binary::Decoder{buffer, recv_bytes}))
			break;

		/**
		 * TODO(gertsog@gmail.com): may be need send a reply that all OK or Error
		 *   It will require extend SocketSender to add another one constructor
		 *   to accept already connected network::SocketPtr object.
		 */
	}

	VERBOSE(1, "Close conection to peer fd=", socket->fd());
}

auto createServerSocket()
{
	network::Socket::Info info;
	info.address = Config::instance().local_address;  // bind server to local address
	info.server_backlog = 1;                          // set size the queue of pending connections
	info.block_timeout_ms = -1;                       // turn off any handling blocked mechanism
	auto server = network::Socket::create(info);
	server->open();
	VERBOSE(1, "Start server on '%'", info.address);
	return server;
}

bool handleConnection(network::SocketPtr peer)
{
	if(!peer)
		return false;

	VERBOSE(1, "<*> New connection fd=", peer->fd());
	std::thread(&recvThread, std::move(peer)).detach();
	return true;
}

}  // namespace

void listenThread()
{
	VERBOSE(1, "Listen thread started");
	auto server = createServerSocket();

	while(handleConnection(server->accept()))
		;

	VERBOSE(1, "Stop listen thread");
}
