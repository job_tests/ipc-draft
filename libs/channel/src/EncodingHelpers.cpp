#include "channel/EncodingHelpers.h"

namespace channel::encoding {
std::string decode(BaseDecoder &input, Value<std::string> v)
{
	auto len = input.load<StringLengthType>(v.key);
	char const *str = input.end();
	input.shift(len);
	return {str, len};
}

}  // namespace channel::encoding
