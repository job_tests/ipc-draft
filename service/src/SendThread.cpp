#include <chrono>
#include <thread>
#include <unistd.h>
#include "channel/BinaryEncoder.h"
#include "models/SimpleData.h"
#include "models/StringData.h"
#include "config.h"
#include "logger.h"
#include "prototcol_tags.h"
#include "SocketSender.h"

namespace {

void sayHello(channel::SenderPtr sender)
{
	channel::binary::Encoder encoder(sender);
	encoder.save(tag::DataVersion, tag::Version);
	encoder.save(Config::instance().client_name, tag::ClientName);
	encoder.save(getpid(), tag::ClientId);
}

void sayBye(channel::SenderPtr sender)
{
	channel::binary::Encoder encoder(sender);
	encoder.save(Config::instance().client_name, tag::Exit);
}

void pause(int ms)
{
	if(ms > 0)
		std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

models::SimpleData makeSimpleData(int idx)
{
	// Some kind of initialisation, just to fill pseudo data
	return {char(idx & 0xFF), idx, double(idx) / 8, uint64_t(idx * idx)};
}

models::StringData makeStringData(int idx)
{
	// Some kind of initialisation, just to fill pseudo data
	models::StringData data;
	data.str = std::string(idx % 8, 'a' + idx % 26);

	int const Count = idx % 10;
	for(int i = 0; i < Count; ++i)
		data.vec_str.emplace_back((idx + i) % 10, 'A' + idx % 26);

	return data;
}

void sendDataPacket(std::shared_ptr<SocketSender> sender, int idx)
{
	VERBOSE(2, "counter=", idx);
	channel::binary::Encoder encoder(sender);
	if(idx & 1)
		encoder.save(makeStringData(idx), models::StringData::DefaultName);
	else
		encoder.save(makeSimpleData(idx), models::SimpleData::DefaultName);
}

}  // namespace

void sendThread()
{
	int constexpr WaitUntilLocalServerReadyMs = 100;

	auto &config = Config::instance();
	if(!config.local_address.empty())
		pause(WaitUntilLocalServerReadyMs);

	auto sender = std::make_shared<SocketSender>(config.remote_address);
	sayHello(sender);

	for(int i = 0; i < config.count; ++i) {
		sendDataPacket(sender, i);
		pause(config.timeout);
	}
	sayBye(sender);
}
