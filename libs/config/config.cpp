#include <cassert>
#include <memory>
#include <iostream>
#include <cxxopts.hpp>
#include "config.h"

namespace {

void createOptionsParser(cxxopts::Options *options, Config *config)
{
	options->add_options()
	    // Help
	    ("h,help", "Print help")
	    // Multiple times to raise debug level
	    ("v,verbose", "Increase verbose output")
	    //
	    ("l,listen",
	     "Supply local service at address",
	     cxxopts::value<std::string>(config->local_address)->implicit_value("localhost:1234"),
	     "address")
	    //
	    ("r,remote",
	     "Connect to remote service",
	     cxxopts::value<std::string>(config->remote_address)->implicit_value("localhost:1234"),
	     "address")
	    //
	    ("n,name",
	     "Client name to remote connection",
	     cxxopts::value<std::string>(config->client_name)->default_value("Client"))
	    //
	    ("c,count", "Sent data <count> times", cxxopts::value<int>(config->count)->default_value("8"))
	    //
	    ("t,timeout", "Delay between send", cxxopts::value<int>(config->timeout)->default_value("10"), "ms")
	    //
	    ;
}

enum class ParseStatus
{
	Ok,
	Fail,
	NeedHelp
};

template<typename T>
ParseStatus parseCommandParams(T &&options, Config *config)
{
	if(options.count("help"))
		return ParseStatus::NeedHelp;

	config->verbose = options.count("verbose");

	return ParseStatus::Ok;
}

}  // namespace

Config::Config(int argc, char *argv[])
{
	assert(argc && argv && "First call of Config::instance() should be done from main()");

	ParseStatus status = ParseStatus::Ok;
	cxxopts::Options options(argv[0], "Simple IPC");

	try {
		createOptionsParser(&options, this);
		auto result = options.parse(argc, argv);
		status = parseCommandParams(std::move(result), this);

	} catch(std::exception const &e) {
		status = ParseStatus::Fail;
		std::cerr << "Error: Can't parse command line: " << e.what() << std::endl;
	}

	if(status != ParseStatus::Ok) {
		std::cout << options.help() << std::endl;
		exit(status == ParseStatus::NeedHelp ? 0 : -1);
	}
}

/*static*/ Config const &Config::instance(int argc /*=0*/, char *argv[] /*=nullptr*/) noexcept
{
	static Config config(argc, argv);
	return config;
}
