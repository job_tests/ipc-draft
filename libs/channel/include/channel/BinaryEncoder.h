#pragma once
#include <cinttypes>
#include <sstream>
#include <vector>
#include "BaseEncoders.h"

namespace channel::binary {

class Encoder final : public BaseEncoder
{
public:
	Encoder(SenderPtr sender, int expected_size = 0);
	virtual ~Encoder();

	virtual void append(void const *data, size_t len, std::string_view key) override;

	void send();
	void reset() { buffer.clear(); }

protected:
	virtual void add(void const *data, size_t len);
	virtual void sendBatch();

	std::vector<char> buffer;

private:
	void checkSend() noexcept;
};

}  // namespace channel::binary
