#pragma once
#include "format.h"
#include "config.h"

void printMessage(std::string const &message);

#define VERBOSE(level, ...)                                                       \
	if(Config::instance().verbose >= level)                                       \
	printMessage(std::string(__FUNCTION__) + ':' + std::to_string(__LINE__) + " " \
	             + std::string(Log::Format{__VA_ARGS__}))
