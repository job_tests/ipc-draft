#pragma once
#include <sys/un.h>
#include "Socket.h"

namespace network {

char const AbstractUnixSocketSign = '|';

class UnixSocket : public Socket
{
public:
	UnixSocket(Info const &si);

	virtual void open() override;

	static bool is_socket(std::string const &address);

private:
	sockaddr_un addr;
};

}  // namespace network
