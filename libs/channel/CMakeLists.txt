project(channel LANGUAGES CXX VERSION 0.0.1)

add_library(${PROJECT_NAME} STATIC
	src/BinaryEncoder.cpp
	src/EncodingHelpers.cpp
)
target_include_directories(${PROJECT_NAME} PUBLIC include)
