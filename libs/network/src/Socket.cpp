#include <cassert>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <string>
#include <time.h>
#include <linux/net_tstamp.h>
#include <linux/errqueue.h>
#include <linux/sockios.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "network/UnixSocket.h"
#include "network/TcpSocket.h"
#include "network/Socket.h"

namespace network {

namespace {

void setBlockMode(int fd, bool non_block)
{
	int flags = fcntl(fd, F_GETFL);
	if(flags == -1)
		throw SocketException({"fcntl(%, F_GETFL) before setting O_NONBLOCK: %", fd, strerror(errno)});

	if(non_block)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;

	if(fcntl(fd, F_SETFL, flags) == -1)
		throw SocketException({"fcntl(%, F_SETFL, O_NONBLOCK): %", fd, strerror(errno)});
}

void setSocketTimeout(int fd, time_t block_timeout_ms)
{
	timeval timeout = {block_timeout_ms / 1000, (block_timeout_ms % 1000) * 1000};
	if(setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)))
		throw SocketException({"Cannot set SO_SNDTIMEO to fd=%: %", fd, strerror(errno)});
	if(setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)))
		throw SocketException({"Cannot set SO_RCVTIMEO to fd=%: %", fd, strerror(errno)});
}

void setSocketTimestamping(int fd)
{
	int ts_flags = SOF_TIMESTAMPING_RX_SOFTWARE | SOF_TIMESTAMPING_SOFTWARE | SOF_TIMESTAMPING_RAW_HARDWARE
	               | SOF_TIMESTAMPING_RX_HARDWARE;
	if(setsockopt(fd, SOL_SOCKET, SO_TIMESTAMPING, &ts_flags, sizeof(ts_flags)))
		throw SocketException({"Cannot set SO_TIMESTAMPING to fd=%: %", fd, strerror(errno)});
}

bool checkIsNetworkAddress(Socket::Info const &si)
{
	if(si.port > 0)
		return true;

	if(UnixSocket::is_socket(si.address))
		return false;

	return true;
}

void getTimestamping(msghdr *msg, timespec *ts)
{
	if(!ts)
		return;

	scm_timestamping *tss = nullptr;
	for(cmsghdr *cmsg = CMSG_FIRSTHDR(msg); cmsg; cmsg = CMSG_NXTHDR(msg, cmsg)) {
		if(cmsg->cmsg_level == SOL_SOCKET && cmsg->cmsg_type == SCM_TIMESTAMPING) {
			tss = reinterpret_cast<scm_timestamping *>(CMSG_DATA(cmsg));
			break;
		}
	}
	if(tss)
		*ts = tss->ts[0];  // Use 'soft' timestamp, to avoid time sync
}

}  // namespace

Socket::Socket(Info const &si)
    : sock_fd(-1)
    , use_timestamping(si.use_timestamping)
    , block_timeout_ms(si.block_timeout_ms)
{}

/*virtual*/ int Socket::makeSocket(int domain, int type, int protocol)
{
	if(sock_fd != -1)
		throw SocketException({"Wrong app flow: already opened (%)", sock_fd});

	int fd = socket(domain, type, protocol);
	if(fd == -1)
		throw SocketException({"Can't create socket: ", strerror(errno)});
	if(block_timeout_ms > 0)
		setSocketTimeout(fd, block_timeout_ms);
	return fd;
}

void Socket::open(int fd)
{
	if(fd != -1)
		try {
			setBlockMode(fd, block_timeout_ms == 0);
		} catch(SocketException const &e) {
			close(fd);
			throw;
		}
	sock_fd = fd;
	if(use_timestamping)
		setSocketTimestamping(sock_fd);
}

/*virtual*/ void Socket::close()
{
	close(sock_fd);
	sock_fd = -1;
}

void Socket::close(int fd)
{
	if(fd != -1)
		shutdown(fd, SHUT_RDWR);
}

/*virtual*/ ssize_t Socket::recv(void *data, ssize_t count_bytes)
{
	ssize_t nbytes = ::recv(sock_fd, data, count_bytes, 0);
	if(nbytes < 0 && errno != EAGAIN)
		throw SocketException({"%: '%'", errno, strerror(errno)});
	return nbytes > 0 ? nbytes : 0;
}

/*virtual*/ ssize_t Socket::recv(void *data, ssize_t count_bytes, timespec *ts)
{
	return use_timestamping ? ts_recv_recvmsg(data, count_bytes, ts) : ts_recv_ioctl(data, count_bytes, ts);
}

ssize_t Socket::ts_recv_recvmsg(void *data, ssize_t count_bytes, timespec *ts)
{
	int constexpr ControlMessageSize = CMSG_SPACE(sizeof(timeval) + 3 * sizeof(timespec) + sizeof(uint32_t));
	char ctrlmsg[ControlMessageSize];
	iovec iov{data, size_t(count_bytes)};
	msghdr msg{0};
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = &ctrlmsg;
	msg.msg_controllen = sizeof(ctrlmsg);
	ssize_t nbytes = recvmsg(sock_fd, &msg, 0);

	if(nbytes < 0 && errno != EAGAIN)
		throw SocketException({"%: '%'", errno, strerror(errno)});
	else if(nbytes > 0)
		getTimestamping(&msg, ts);

	return nbytes > 0 ? nbytes : 0;
}

ssize_t Socket::ts_recv_ioctl(void *data, ssize_t count_bytes, timespec *ts)
{
	auto size = recv(data, count_bytes);
	if(ts) {
		timeval tv;
		if(!ioctl(sock_fd, SIOCGSTAMP, &tv)) {
			ts->tv_sec = tv.tv_sec;
			ts->tv_nsec = tv.tv_usec * 1000;
		}
	}
	return size;
}

/*virtual*/ ssize_t Socket::send(void const *data, ssize_t count_bytes)
{
	ssize_t nbytes = ::send(sock_fd, data, count_bytes, 0);
	if(nbytes < 0 && errno != EAGAIN)
		throw SocketException({"%: '%'", errno, strerror(errno)});
	return nbytes > 0 ? nbytes : 0;
}

/*static*/ SocketPtr Socket::create(Info const &si)
{
	switch(si.family) {
		case Family::Unix: return std::make_unique<UnixSocket>(si);
		case Family::Tcp: return std::make_unique<TcpSocket>(si);
		case Family::TcpOrUnix:
			if(checkIsNetworkAddress(si))
				return std::make_unique<TcpSocket>(si);
			else
				return std::make_unique<UnixSocket>(si);
			break;
		case Family::UdpOrUnix:
			break;
			if(!checkIsNetworkAddress(si))
				return std::make_unique<UnixSocket>(si);
			break;
		default: assert(!"Unsuported Socket family"); break;
	}
	return SocketPtr();
}

}  // namespace network
