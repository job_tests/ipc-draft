#pragma once
#include <string_view>
#include <cinttypes>
#include "channel/EncodingHelpers.h"

namespace models {

struct SimpleData
{
	static std::string_view constexpr DefaultName{"SimpleData"};

	char c_value = 0;
	int i_value = 0;
	double f_value = 0;
	uint64_t u64_value = 0;

	friend DECLARE_ENCODER(SimpleData)
	{
		output.append(nullptr, 0, e.key);
		output.save(data.c_value, "byte")
		    .save(data.i_value, "int")
		    .save(data.f_value, "real")
		    .save(data.u64_value, "u64");
	}

	friend DECLARE_DECODER(SimpleData)
	{
		if(!input.findEntry(e.key))
			throw channel::Exception({"Not found object key: ", e.key});

		return {
		    input.load<decltype(SimpleData::c_value)>("byte"),
		    input.load<decltype(SimpleData::i_value)>("int"),
		    input.load<decltype(SimpleData::f_value)>("real"),
		    input.load<decltype(SimpleData::u64_value)>("u64"),
		};
	}

	friend std::ostream &operator<<(std::ostream &output, SimpleData const &data)
	{
		output << data.DefaultName << " {\n";
		output << "\tbyte: " << int(data.c_value) << ",\n";
		output << "\tint: " << data.i_value << ",\n";
		output << "\treal: " << data.f_value << ",\n";
		output << "\tu64: " << data.u64_value << "\n";
		output << "}\n";
		return output;
	}
};

}  // namespace models
