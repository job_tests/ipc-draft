#pragma once
#include <stdexcept>
#include "format.h"

namespace channel {

class Exception : public std::runtime_error
{
public:
	Exception(::Log::Format const &fmt)
	    : std::runtime_error(fmt)
	{}
};

}  // namespace channel
