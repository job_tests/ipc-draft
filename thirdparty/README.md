# Внешние зависимости
В проекте используется только одна сторонняя библиотека - для парсинга аргументов командной строки.

## [cxxopts](https://github.com/jarro2783/cxxopts)
Данная библиотека (header only) позволяет заменить комбинацию библиотечных методов POSIX `getopt()` и GNU расширение `getopt_long` на один _С++_ класс для работы параметрами переданными из командной строки. Подобное решение удобно использовать особенно если требуется кроссплатформенность.

Данная библиотека, с одной стороны, привносит удобство, но, к сожалению, не лишена и багов. Так, в частности, нет полноценной поддержки общепринятого формата указания опций, есть много проблем с обработкой коротких (однобуквенных) опций. Для тестовых примеров она вполне подходит, чтобы не тратить лишнее время и код на разбор аргументов, но для реальных приложений лучше не использовать.
