#pragma once
#include <cstdint>
#include <memory>
#include <string>
#include <stdexcept>
#include <netinet/in.h>

#include "format.h"

namespace network {

class SocketException : public std::runtime_error
{
public:
	SocketException(::Log::Format const &fmt)
	    : std::runtime_error(fmt)
	{}
};

class Socket;

using SocketPtr = std::unique_ptr<Socket>;

class Socket
{
public:
	enum class Family
	{
		Unix,
		Tcp,
		Udp,
		Can,
		TcpOrUnix,
		UdpOrUnix,
	};

	struct Info
	{
		std::string address;
		int port = -1;
		Family family = Family::TcpOrUnix;
		time_t block_timeout_ms = 0;  ///< =0 - non blocked mode; >0 set timeout for blocked calls
		int server_backlog = 0;
		bool is_broadcast = false;
		bool use_timestamping = false;
	};

	static SocketPtr create(Info const &si);

	int fd() const { return sock_fd; }

	virtual ~Socket() { close(); }
	virtual void open() = 0;
	virtual void close();
	virtual ssize_t send(void const *data, ssize_t count_bytes);
	virtual ssize_t recv(void *data, ssize_t count_bytes);

	/// 'timespec' - if was set Info::use_timestamping flag (for UDP, RAW) used fast mechanism, other way second ioctl
	virtual ssize_t recv(void *data, ssize_t count_bytes, timespec *ts);

	virtual SocketPtr accept() { throw SocketException("Not applicable for this socket type"); }

	/// `fromAddr` - valid only for DGRAM family sockets and called after `recv`
	virtual sockaddr const *fromAddr(socklen_t *addr_len) const { return nullptr; }

	bool isBlocked() const { return block_timeout_ms != 0; }

	Socket(Socket const &) = delete;
	Socket &operator=(Socket const &) = delete;

protected:
	Socket(Info const &si);
	int makeSocket(int domain, int type, int protocol);
	void open(int fd);
	void close(int fd);

	int sock_fd;
	bool const use_timestamping;
	uint32_t block_timeout_ms;

private:
	ssize_t ts_recv_ioctl(void *data, ssize_t count_bytes, timespec *ts);
	ssize_t ts_recv_recvmsg(void *data, ssize_t count_bytes, timespec *ts);

};

}  // namespace network
