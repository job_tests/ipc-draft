#include <cassert>
#include <errno.h>
#include <string.h>
#include <string>

#include "network/TcpSocket.h"

namespace network {

/*virtual*/ int TcpSocket::onOpen(ListIpAddress const &list_ip) /*override*/
{
	int fd = makeSocket(AF_INET, SOCK_STREAM, 0);
	if(isServer())
		bindToAddress(fd, list_ip);
	else
		connectToAddress(fd, list_ip);
	return fd;
}

void TcpSocket::connectToAddress(int fd, ListIpAddress const &list_ip)
{
	uint16_t port = ntohs(addr.sin_port);
	for(auto ip : list_ip) {
		addr.sin_addr.s_addr = ip;
		if(connect(fd, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) == 0)
			return;
	}
	close(fd);
	throw SocketException({"Can't connect to '%:%' (%)", address, port, strerror(errno)});
}

void TcpSocket::bindToAddress(int fd, ListIpAddress const &list_ip)
{
	addr.sin_addr.s_addr = list_ip[0];
	if(bind(fd, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) != 0)
		throw SocketException({"Can't bind to '%:%' (%)", address, ntohs(addr.sin_port), strerror(errno)});

	if(listen(fd, server_backlog) != 0)
		throw SocketException({"Can't set listen for '%:%' (%)", address, ntohs(addr.sin_port), strerror(errno)});
}

TcpSocket::TcpSocket(TcpSocket const *const server)
    : IpSocket({"", server->addr.sin_port, Family::Tcp, server->block_timeout_ms, 0, false, server->use_timestamping})
    , peer(std::make_unique<FromTcpAddres>())
{
	int new_fd = ::accept(server->fd(), &peer->addr, &peer->len);
	if(new_fd == -1)
		throw SocketException(
		    {"Can't accept connection on '%:%' (%)", server->address, ntohs(addr.sin_port), strerror(errno)});
	Socket::open(new_fd);
}

/*virtual*/ SocketPtr TcpSocket::accept() /*override*/
{
	struct workaround final : public TcpSocket
	{
		// Just to allow create instance with smart pointer from private constructor
		workaround(TcpSocket const *const server)
		    : TcpSocket{server}
		{}
	};
	return std::make_unique<workaround>(this);
}

/*virtual*/ sockaddr const *TcpSocket::fromAddr(socklen_t *addr_len) const /*override*/
{
	if(!peer)
		return nullptr;

	if(addr_len)
		*addr_len = peer->len;
	return &peer->addr;
}

}  // namespace network
