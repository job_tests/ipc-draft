#pragma once
#include <ostream>
#include <string>
#include <vector>
#include "channel/EncodingHelpers.h"

namespace models {

struct StringData
{
	static std::string_view constexpr DefaultName{"StringData"};

	std::string str;
	std::vector<std::string> vec_str;

	friend DECLARE_ENCODER(StringData)
	{
		output.append(nullptr, 0, e.key);
		output.save(data.str, "str").save(data.vec_str, "list");
	}

	friend DECLARE_DECODER(StringData)
	{
		if(!input.findEntry(e.key))
			throw channel::Exception({"Not found object key: ", e.key});

		auto str = input.load<decltype(StringData::str)>("str");
		auto vec = input.load<decltype(StringData::vec_str)>("list");
		return {std::move(str), std::move(vec)};
	}

	friend std::ostream &operator<<(std::ostream &output, StringData const &data)
	{
		output << data.DefaultName << " {\n";
		output << "\tstr: \"" << data.str << ",\"\n";
		output << "\tlist: [\n";
		for(auto &s : data.vec_str)
			output << "\t\t\"" << s << "\",\n";
		output << "\t]\n";
		output << "}\n";
		return output;
	}
};

}  // namespace models
