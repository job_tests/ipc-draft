#pragma once
#include "channel/Sender.h"
#include "network/Socket.h"
#include "logger.h"

class SocketSender final : public channel::Sender
{
public:
	SocketSender(std::string const &remote_address)
	    : sock()
	{
		sock = network::Socket::create({.address = remote_address});
		sock->open();
	}

	virtual void send(void const *data, size_t len) override
	{
		VERBOSE(2, "data=%, size=% bytes; tag=%", data, len, (char const *)data);
		sock->send(data, len);
	}

	auto recv(void *data, size_t count_bytes) { return sock->recv(data, count_bytes); }

private:
	network::SocketPtr sock;
};
