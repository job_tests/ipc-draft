#include <iostream>
#include <string>
#include <mutex>
#include "logger.h"

namespace {
std::mutex on_cerr;
}

void printMessage(std::string const &message)
{
	on_cerr.lock();
	std::cerr << message << std::endl;
	on_cerr.unlock();
}
