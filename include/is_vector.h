#pragma once
#include <vector>
#include <type_traits>

template<typename T, typename = void>
struct is_vector : std::false_type
{};

template<typename... Args>
struct is_vector<std::vector<Args...>> : std::true_type
{};

template<class T>
constexpr bool is_vector_v = is_vector<T>::value;
