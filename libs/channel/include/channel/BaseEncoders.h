#pragma once
#include <cstddef>
#include <vector>
#include <string_view>
#include "Sender.h"

namespace channel {
namespace encoding {

struct Key
{
	std::string_view key;
};

template<typename T>
struct Value : public Key
{};

}  // namespace encoding

/**
 *
 */
class BaseDecoder
{
public:
	virtual ~BaseDecoder() {}
	virtual char const *end() const noexcept = 0;
	virtual int length() const noexcept = 0;
	virtual void shift(size_t element_size) = 0;
	virtual bool findEntry(std::string_view key) = 0;

	template<typename T>
	T load(std::string_view key)
	{
		return decode(*this, encoding::Value<T>{key});
	}
};

/**
 *
 */
class BaseEncoder
{
public:
	BaseEncoder(SenderPtr sender)
	    : sender(sender)
	{}

	virtual ~BaseEncoder() {}
	virtual void append(void const *data, size_t len, std::string_view key) = 0;

	template<typename T>
	BaseEncoder &save(T &&val, std::string_view key)
	{
		encode(*this, std::forward<T>(val), encoding::Value<T>{key});
		return *this;
	}

	template<typename T>
	BaseEncoder &save(T const &val, std::string_view key)
	{
		encode(*this, val, encoding::Value<T>{key});
		return *this;
	}

protected:
	SenderPtr sender;
};

}  // namespace channel

/**
 * TODO(gertsog@gmail.com): Implement some macros magic witn variadic arguments to wrap both functions with their body:
 *   ex.: DECLARE_ENCODER(Class, field1, field2, field3, ...)
 *   may be it can be done with BOOST_FOREACH(...)
 */
#define DECLARE_ENCODER(Class) void encode(channel::BaseEncoder &output, Class const &data, channel::encoding::Value<Class> e)
#define DECLARE_DECODER(Class) Class decode(channel::BaseDecoder &input, channel::encoding::Value<Class> e)
