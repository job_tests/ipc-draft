#pragma once
#include <string>

struct Config
{
	int verbose = 0;
	std::string local_address;
	std::string client_name;
	std::string remote_address;
	int count = 0;
	int timeout = 0;

	static Config const &instance(int argc = 0, char *argv[] = nullptr) noexcept;

	Config(Config const &) = delete;
	Config &operator=(Config const &) = delete;

private:
	Config(int argc, char *argv[]);
};
