#pragma once
#include <vector>
#include <netinet/in.h>
#include "Socket.h"

namespace network {

using ListIpAddress = std::vector<in_addr_t>;

class IpSocket : public Socket
{
public:
	IpSocket(Info const &si);
	virtual void open() override;

protected:
	virtual int onOpen(ListIpAddress const &list_ip) = 0;
	bool isServer() const { return server_backlog > 0; }

	sockaddr_in addr;
	std::string address;
	int server_backlog;
};

}  // namespace network
